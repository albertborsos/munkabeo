<?php

namespace backend\controllers;

use Yii;
use backend\models\Jelenleti;
use backend\models\JelenletiSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\JelenletiTipus;
use backend\models\Alkalmazottak;

/**
 * JelenletiController implements the CRUD actions for Jelenleti model.
 */
class JelenletiController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Jelenleti models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new JelenletiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);


        $JelenletiTipus = new JelenletiTipus();
                if ($JelenletiTipus->load(Yii::$app->request->post()) && $JelenletiTipus->validate()) {
                        Yii::$app->session->setFlash('success',
                    'The form was successfully processed!'
            );
        }
        return $this->render('index', array(
        'jelenleti_tipus_id' => $JelenletiTipus,
           ));

        $Alkalmazottak = new Alkalmazottak();
           if ($Alkalmazottak->load(Yii::$app->request->post()) && $Alkalmazottak->validate()) {
                   Yii::$app->session->setFlash('success',
               'The form was successfully processed!'
            );
        }
   
        return $this->render('index', array(
        'Alkalmazottak' => $JelenletiTipus,
      ));  
    
    
    
    }  

    /**
     * Displays a single Jelenleti model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Jelenleti model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Jelenleti();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Jelenleti model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Jelenleti model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    



    /**
     * Finds the Jelenleti model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Jelenleti the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Jelenleti::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
