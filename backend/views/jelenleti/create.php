<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Jelenleti */

$this->title = 'Create Jelenleti';
$this->params['breadcrumbs'][] = ['label' => 'Jelenletis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jelenleti-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
