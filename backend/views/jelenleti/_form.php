<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\JelenletiTipus;
use backend\models\Alkalmazottak;
use kartik\select2\Select2;
use dosamigos\datepicker\DatePicker;
 


/* @var $this yii\web\View */
/* @var $model backend\models\Jelenleti */
/* @var $jelenleti_tipus backend\models\JelenletiTipus */
/* @var $jelenleti_tipus backend\models\Alkalmazottak */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jelenleti-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?=$form->field($model, 'alkalmazottak_nev')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Alkalmazottak::find()->all(),'id', 'teljes_nev'),
                'language' => 'en',
                'options' => ['placeholder' => 'Nev kivalasztas ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
   
    <?=$form->field($model, 'alkalmazottak_nev')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Alkalmazottak::find()->all(),'id', 'teljes_nev'),
                'language' => 'en',
                'options' => ['placeholder' => 'Nev kivalasztas ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>

    <?= $form->field($model, 'datum')->widget(
                DatePicker::className(), [                    
                    'inline' => false, 
                    // modify template for custom rendering
                    //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-M-dd'
                    ]
            ]);?>    

    <?= $form->field($model, 'kezdes')->textInput() ?>

    <?= $form->field($model, 'veg')->textInput() ?>
    
    <?= $form->field($model, 'jelenleti_tipus_id')->dropDownList(ArrayHelper::map(JelenletiTipus::find()->all(), 'jelenleti_tipus_id', 'type')); ?>
         
    <?= $form->field($model, 'megjegyzes')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
