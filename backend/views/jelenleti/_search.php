<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\JelenletiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jelenleti-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'alkalmazottak_nev') ?>

    <?= $form->field($model, 'datum') ?>

    <?= $form->field($model, 'kezdes') ?>

    <?= $form->field($model, 'veg') ?>

    <?php // echo $form->field($model, 'jelenleti_tipus_id') ?>

    <?php // echo $form->field($model, 'megjegyzes') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
