<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\OsszesitoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="osszesito-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'alkalmazottak_nev') ?>

    <?= $form->field($model, 'datum_kezdo') ?>

    <?= $form->field($model, 'datum_veg') ?>

    <?= $form->field($model, 'ora_szam') ?>

    <?php // echo $form->field($model, 'jelenleti_tipus_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
