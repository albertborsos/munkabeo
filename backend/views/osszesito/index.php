<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\OsszesitoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Osszesitos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="osszesito-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Osszesito', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'alkalmazottak_nev',
            'datum_kezdo',
            'datum_veg',
            'ora_szam',
            'jelenleti_tipus_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
