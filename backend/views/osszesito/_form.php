<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Osszesito */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="osszesito-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'alkalmazottak_nev')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'datum_kezdo')->textInput() ?>

    <?= $form->field($model, 'datum_veg')->textInput() ?>

    <?= $form->field($model, 'ora_szam')->textInput() ?>

    <?= $form->field($model, 'jelenleti_tipus_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
