<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Osszesito */

$this->title = 'Update Osszesito: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Osszesitos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="osszesito-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
