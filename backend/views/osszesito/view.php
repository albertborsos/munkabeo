<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use sjaakp\gcharts\PieChart;

/* @var $this yii\web\View */
/* @var $model backend\models\Osszesito */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Osszesitos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="osszesito-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'alkalmazottak_nev',
            'datum_kezdo',
            'datum_veg',
            'ora_szam',
            'jelenleti_tipus_id',
        ],
    ]) ?>

    <?= PieChart::widget([
        'height' => '400px',
        'dataProvider' => $dataProvider,
        'columns' => [
            'alkalmazottak_nev:string',
            'ora_szam',
        ],
        'options' => [
            'title' => 'Countries by Population'
        ],
    ]) ?>

</div>
