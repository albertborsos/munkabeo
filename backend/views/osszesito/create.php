<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Osszesito */

$this->title = 'Create Osszesito';
$this->params['breadcrumbs'][] = ['label' => 'Osszesitos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="osszesito-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
