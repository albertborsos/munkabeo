<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\JelenletiTipus */

$this->title = 'Create Jelenleti Tipus';
$this->params['breadcrumbs'][] = ['label' => 'Jelenleti Tipuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jelenleti-tipus-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
