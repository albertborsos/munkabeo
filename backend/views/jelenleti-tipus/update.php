<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\JelenletiTipus */

$this->title = 'Jelenleti modositas';
$this->params['breadcrumbs'][] = ['label' => 'Jelenleti Tipuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->jelenleti_tipus_id, 'url' => ['view', 'id' => $model->jelenleti_tipus_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="jelenleti-tipus-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
