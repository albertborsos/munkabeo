<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\JelenletiTipus */

$this->title = $model->jelenleti_tipus_id;
$this->params['breadcrumbs'][] = ['label' => 'Jelenleti Tipuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jelenleti-tipus-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->jelenleti_tipus_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->jelenleti_tipus_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'jelenleti_tipus_id',
            'type',
        ],
    ]) ?>

</div>
