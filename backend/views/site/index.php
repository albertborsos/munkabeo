<?php

/* @var $this yii\web\View */

$this->title = 'Fo oldal';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Dolgozói nyilvántartás</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="/index.php?r=alkalmazottak%2Findex">Alkalmazottak</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Alkalmazottak</h2>

                <p>Jelen cégnél dolgozok listája és regisztrálása</p>

                <p><a class="btn btn-default" href="/index.php?r=alkalmazottak%2Findex">Felvétel &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Jelenleti</h2>

                <p>Jelenleti nyilvántartás</p>

                <p><a class="btn btn-default" href="/index.php?r=jelenleti%2Findex">Jelenleti  &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Összesítő</h2>

                <p>Megadott időpontban ki mennyit dolgozott.</p>

                <p><a class="btn btn-default" href="/index.php?r=osszesito%2Findex">Yii Extensions &raquo;</a></p>
            </div>
        </div>

    </div>
</div>
