<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Alkalmazottak */

$this->title = 'Alkalmazottak';
$this->params['breadcrumbs'][] = ['label' => 'Alkalmazottaks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alkalmazottak-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
