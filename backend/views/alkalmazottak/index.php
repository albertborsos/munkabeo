<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\AlkalmazottakSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Alkalmazott regisztracio.';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alkalmazottak-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::button('Uj alkalmazott felvetele', ['value'=>Url::to('index.php?r=alkalmazottak%2Fcreate'),'class' => 'btn btn-success','id'=>'modalButton']) ?>
    </p>

    <?php
        Modal::begin([
                'header'=>'<h4>Alkalmazott</h4>',
                'id'=>'modal',
                'size'=>'modal-lg',
            ]);
        echo "<div id='modalContent'></div>";
        
            Modal::end();
    ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',            
            'teljes_nev',
            'cim',
            'telefonszam',
            'mail',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
