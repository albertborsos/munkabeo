<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Alkalmazottak */

$this->title = 'Alkalmazottak frissites';
$this->params['breadcrumbs'][] = ['label' => 'Alkalmazottaks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="alkalmazottak-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
