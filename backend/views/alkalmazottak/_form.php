<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Alkalmazottak */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="alkalmazottak-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'teljes_nev')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cim')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'telefonszam')->textInput() ?>

    <?= $form->field($model, 'mail')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
