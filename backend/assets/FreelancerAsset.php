<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class FreelancerAsset extends AssetBundle
{
    public $sourcePath = '@backend/assets/freelancer';

    public $css = [
        'css/freelancer.css',
    ];
    public $js = [
        'js/contact_me.js',
        'js/freelancer.js',
        'js/jqBootstrapValidation.js',
    ];

    public $depends = [
        'yii\bootstrap\BootstrapAsset',
    ];
}
