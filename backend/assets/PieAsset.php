<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class PieAsset extends AssetBundle
{
    public $sourcePath = '@backend/assets/chart';

    public $css = [
        'css/all.css',
    ];
}
