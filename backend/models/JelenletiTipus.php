<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "jelenleti_tipus".
 *
 * @property int $jelenleti_tipus_id
 * @property string $type
 */
class JelenletiTipus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jelenleti_tipus';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            [['type'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'jelenleti_tipus_id' => 'Jelenleti Tipus ID',
            'type' => 'Type',
        ];
    }
}
