<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "alkalmazottak".
 *
 * @property int $id
 * @property string $vezetek_nev
 * @property string $kereszt_nev
 * @property string $cim
 * @property int $telefonszam
 * @property string $mail
 */
class Alkalmazottak extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'alkalmazottak';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['teljes_nev', 'cim', 'telefonszam', 'mail'], 'required'],
            [['telefonszam'], 'integer'],
            [['teljes_nev'], 'string', 'max' => 100],
            [['cim'], 'string', 'max' => 255],
            [['mail'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'teljes_nev' => 'Teljes Nev',
            'cim' => 'Cim',
            'telefonszam' => 'Telefonszam',
            'mail' => 'Mail',
        ];
    }
}
