<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "jelenleti".
 *
 * @property int $id
 * @property int $alkalmazottak_nev
 * @property string $datum
 * @property string $kezdes
 * @property string $veg
 * @property int $jelenleti_tipus_id
 * @property string $megjegyzes
 */
class Jelenleti extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jelenleti';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['alkalmazottak_nev', 'datum', 'kezdes', 'veg', 'jelenleti_tipus_id'], 'required'],
            [['jelenleti_tipus_id'], 'integer'],
            [['datum', 'alkalmazottak_nev', 'kezdes', 'veg', 'megjegyzes'], 'safe'],
            [['megjegyzes'], 'string'],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alkalmazottak_nev' => 'Alkalmazott neve',
            'datum' => 'Datum',
            'kezdes' => 'Kezdes',
            'veg' => 'Veg',
            'jelenleti_tipus_id' => 'Jelenleti Tipus',
            'megjegyzes' => 'Megjegyzes',
        ];
    }
}
