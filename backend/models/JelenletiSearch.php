<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Jelenleti;

/**
 * JelenletiSearch represents the model behind the search form of `backend\models\Jelenleti`.
 */
class JelenletiSearch extends Jelenleti
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'alkalmazottak_nev', 'jelenleti_tipus_id'], 'integer'],
            [['datum', 'kezdes', 'veg', 'megjegyzes'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Jelenleti::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'alkalmazottak_nev' => $this->alkalmazottak_nev,
            'datum' => $this->datum,
            'kezdes' => $this->kezdes,
            'veg' => $this->veg,
            'jelenleti_tipus_id' => $this->jelenleti_tipus_id,
        ]);

        $query->andFilterWhere(['like', 'megjegyzes', $this->megjegyzes]);

        return $dataProvider;
    }
}
