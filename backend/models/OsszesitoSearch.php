<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Osszesito;

/**
 * OsszesitoSearch represents the model behind the search form of `backend\models\Osszesito`.
 */
class OsszesitoSearch extends Osszesito
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'ora_szam', 'jelenleti_tipus_id'], 'integer'],
            [['alkalmazottak_nev', 'datum_kezdo', 'datum_veg'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Osszesito::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'datum_kezdo' => $this->datum_kezdo,
            'datum_veg' => $this->datum_veg,
            'ora_szam' => $this->ora_szam,
            'jelenleti_tipus_id' => $this->jelenleti_tipus_id,
        ]);

        $query->andFilterWhere(['like', 'alkalmazottak_nev', $this->alkalmazottak_nev]);

        return $dataProvider;
    }
}
