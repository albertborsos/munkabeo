<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "osszesito".
 *
 * @property int $id
 * @property string $alkalmazottak_nev
 * @property string $datum_kezdo
 * @property string $datum_veg
 * @property int $ora_szam
 * @property int $jelenleti_tipus_id
 */
class Osszesito extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'osszesito';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['alkalmazottak_nev', 'datum_kezdo', 'datum_veg', 'ora_szam', 'jelenleti_tipus_id'], 'required'],
            [['datum_kezdo', 'datum_veg'], 'safe'],
            [['ora_szam', 'jelenleti_tipus_id'], 'integer'],
            [['alkalmazottak_nev'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alkalmazottak_nev' => 'Alkalmazottak Nev',
            'datum_kezdo' => 'Datum Kezdo',
            'datum_veg' => 'Datum Veg',
            'ora_szam' => 'Ora Szam',
            'jelenleti_tipus_id' => 'Jelenleti Tipus ID',
        ];
    }
}
