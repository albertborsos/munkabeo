<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Alkalmazottak;

/**
 * AlkalmazottakSearch represents the model behind the search form of `backend\models\Alkalmazottak`.
 */
class AlkalmazottakSearch extends Alkalmazottak
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'telefonszam'], 'integer'],
            [['teljes_nev', 'cim', 'mail'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Alkalmazottak::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'telefonszam' => $this->telefonszam,
        ]);

        $query->andFilterWhere(['like', 'teljes_nev', $this->teljes_nev])            
            ->andFilterWhere(['like', 'cim', $this->cim])
            ->andFilterWhere(['like', 'mail', $this->mail]);

        return $dataProvider;
    }
}
