FROM yiisoftware/yii2-php:7.1-apache

RUN apt-get update && \
    apt-get -y install \
    curl git bzip2 openssh-client rsync --no-install-recommends \
    && rm -rf /var/lib/apt/lists/*

COPY docker/default.conf /etc/apache2/sites-enabled/000-default.conf

COPY docker/testport.conf /etc/apache2/conf-available/testport.conf
RUN a2enconf testport

RUN a2enmod rewrite

RUN rm -rf /var/www/html

ARG wwwUid=""
ARG wwwGid=""

RUN if [ -n "$wwwUid" ] && [ "$wwwUid" -ge 500 ]; then usermod -u "$wwwUid" www-data; fi
RUN if [ -n "$wwwGid" ] && [ "$wwwGid" -ge 500 ]; then groupmod -g "$wwwGid" www-data; fi

# /var/www is ~www-data, and yarn and composer want to put their caches in there
RUN chown -R www-data:www-data /var/www /usr/local/bin/composer

RUN mkdir -p /var/www/.composerhost
RUN mkdir -p /var/www/.composer
RUN chown -R www-data:www-data /var/www/.composerhost
RUN chown -R www-data:www-data /var/www/.composer
RUN ln -s /var/www/.composerhost/auth.json /var/www/.composer/auth.json
VOLUME /var/www/.composerhost

COPY docker/start-ssh-agent /usr/local/bin
RUN chmod a+x /usr/local/bin/start-ssh-agent

RUN chsh -s /bin/bash www-data

WORKDIR /app

# we need to set the default path for su users since we use su
RUN sed -i 's/ENV_PATH\(\s*\)PATH=\(.*\)/ENV_PATH\1PATH=\/var\/www\/app\/node_modules\/\.bin:\2/' /etc/login.defs

ENV DOCKERIZED=true
ENV APPENV=development
